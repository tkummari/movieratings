package com.movie.ratings
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._


object MovieLense {
  
  def main(args: Array[String]): Unit = {
    
    val movies_file=args(0)
    val ratings_file=args(1)
    val master=args(2)
    val deploy_mode=args(3)
    
    val spark=SparkSession.builder().appName("MovieRatings").master(master).enableHiveSupport().config("spark.sql.shuffle.partitions","50").
    config("spark.serializer","org.apache.spark.serializer.KryoSerializer").
    config("spark.sql.warehouse.dir","C:\\Users\\THIRU\\git\\r8gpm\\Sandbox\\Spark-warehouse").getOrCreate()
    
    val movies_df=spark.read.option("header","true").option("inferschema","true").csv(movies_file)
    val ratings_df=spark.read.option("header","true").option("inferschema","true").csv(ratings_file)
    
    val processedDf=ratings_df.join(movies_df,ratings_df("movieId")===movies_df("movieId")).
    select("userId","title","rating").groupBy("userId").
    agg(max("rating").as("Higest_ratingg"))
    
    processedDf.show(10)
    
    
  }
  
}